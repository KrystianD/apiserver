from typing import TYPE_CHECKING
import inspect

from ..utils import camelcase2underscore
from .api_model import APIModel

if TYPE_CHECKING:
    from ..api_manager import APIManager


class APIClass:
    def __init__(self, mgr: 'APIManager', cls):
        self.mgr = mgr
        self.cls = cls

    @property
    def name(self):
        return self.cls.__name__

    @property
    def filename(self):
        return getattr(self.cls, "API_FILENAME", camelcase2underscore(self.cls.__name__))

    @property
    def endpoint_path(self):
        return self.cls.endpoint_path

    def has_base_class(self):
        return hasattr(self.cls, "DB_CLS")

    def get_base_class(self):
        return APIModel(self.mgr, self.cls.DB_CLS)

    def get_methods(self):
        from .api_method import APIMethod
        methods = []
        for m in inspect.getmembers(self.cls, predicate=inspect.isfunction):
            met = m[1]
            if hasattr(met, "is_endpoint"):
                methods.append(APIMethod(self, met))
        return methods

    @property
    def required_permissions(self):
        return getattr(self.cls, "required_permission", [])
