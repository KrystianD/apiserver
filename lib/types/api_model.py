from typing import TYPE_CHECKING, List, Optional
import enum

from ..api_serialization_context import APIInternalSerializationContext
from .api_field import APIField, APIFieldFlags
from ..utils import camelcase2underscore

if TYPE_CHECKING:
    from ..api_manager import APIManager


class APIModelFlags(enum.IntFlag):
    NoFlag = 0
    AllowRecursion = enum.auto()


# noinspection PyProtectedMember
class APIModel:
    def __init__(self, mgr: 'APIManager', cls):
        self.mgr = mgr
        if isinstance(cls, str):
            model = mgr.find_model_by_name(cls)
            self.cls = model.cls
        else:
            self.cls = cls
        assert self.cls._apimodel is True

    def __str__(self):
        return "[APIModel {0}]".format(self.name)

    def initialize(self):
        for field in self.fields:
            field.initialize(self.mgr, base_class=None)

    @property
    def filename(self):
        return getattr(self.cls, "API_FILENAME", camelcase2underscore(self.cls.__name__))

    @property
    def name(self):
        return self.cls.__name__

    @property
    def fields(self) -> List[APIField]:
        return self.cls.FIELDS

    @property
    def flags(self) -> APIModelFlags:
        return getattr(self.cls, "_apimodelflags", APIModelFlags.NoFlag)

    @property
    def key_field(self) -> Optional[APIField]:
        # key_field = getattr(self.cls, "API_KEY_FIELD", None)
        # if key_field is None:
        #    return None

        fields = self.fields
        for f in fields:
            if APIFieldFlags.PrimaryKey in f.flags:
                return f
        return None

    @property
    def is_internal(self):
        return self.cls._apimodelinternal

    def convert_obj(self, ctx: APIInternalSerializationContext, data):
        out_data = {}
        self.mgr.serializer.convert_obj_fields_to_user(ctx, self.fields, data, out_data)
        return out_data

    def convert_obj_from_user(self, ctx: APIInternalSerializationContext, data):
        out_obj = self.cls()
        self.mgr.serializer.convert_obj_fields_from_user(ctx, self.fields, data, out_obj)
        return out_obj
