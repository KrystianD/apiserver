from typing import Optional, TYPE_CHECKING, List, Any

import enum

if TYPE_CHECKING:
    from .api_model import APIModel
    from ..api_manager import APIManager


class APIFieldType(enum.Enum):
    Integer = 0
    String = 1
    Date = 2
    DateTime = 3
    Key = 4
    List = 5
    Custom = 6
    BaseClass = 7
    Enum = 8
    Decimal = 9
    Binary = 10
    Boolean = 11
    JSON = 12
    Map = 13
    File = 14
    Float = 15


class APIFieldFlags(enum.IntFlag):
    NoFlag = 0
    Reference = enum.auto()
    ToClientOnly = enum.auto()
    AllowRecursion = enum.auto()
    PrimaryKey = enum.auto()


class ConvDirection(enum.Enum):
    FromServer = "fromServer"
    ToServer = "toServer"


def process_type(mgr, type: APIFieldType, custom_type, base_class: 'APIModel') -> 'APIField':
    from .api_model import APIModel

    if type == APIFieldType.Custom:
        if custom_type is not None and not isinstance(custom_type, APIModel):
            if isinstance(custom_type, str):
                custom_type = mgr.find_model_by_name(custom_type)
            elif isinstance(custom_type, APIField):
                return custom_type
            else:
                custom_type = mgr.find_model_by_name(custom_type.__name__)

    elif type == APIFieldType.BaseClass:
        type, custom_type = APIFieldType.Custom, base_class

    elif type == APIFieldType.Enum:
        pass

    else:
        assert custom_type is None

    return APIField(None, type, custom_type)


def build_conv_func(conv: str, direction: ConvDirection, input_variable: str = None, key_only=False):
    if key_only:
        conv = conv + "_key"
    if input_variable is None:
        return "{0}_{1}".format(conv, direction.value)
    else:
        return "{0}_{1}({2})".format(conv, direction.value, input_variable)


class APIField:
    def __init__(self, name: Optional[str], type: APIFieldType, custom_type=None,
                 flags=APIFieldFlags.NoFlag,
                 group: Any = None, groups: List[Any] = None, permissions: List[Any] = None,
                 getter=None, setter=None):
        self.name = name
        self.type = type
        self.custom_type = custom_type
        self.flags = flags
        self.permissions = permissions
        self.getter = getter
        self.setter = setter
        self.groups = set(groups or [])
        if group is not None:
            self.groups.add(group)
        self.mgr: 'APIManager' = None

    def initialize(self, mgr: 'APIManager', base_class: Optional['APIModel']):
        self.mgr = mgr
        tmp_field = process_type(self.mgr, self.type, self.custom_type, base_class)
        self.type, self.custom_type = tmp_field.type, tmp_field.custom_type

    def get_converter_name(self, direction: ConvDirection, input_variable: str = None, key_only=False):
        from .api_model import APIModel

        types_map = {
            APIFieldType.Key: "string",
            APIFieldType.Integer: "int",
            APIFieldType.Float: "float",
            APIFieldType.String: "string",
            APIFieldType.Decimal: "Decimal",
            APIFieldType.Date: "Date",
            APIFieldType.DateTime: "DateTime",
            APIFieldType.Binary: "Uint8Array",
            APIFieldType.Boolean: "boolean",
            APIFieldType.JSON: "any",
            APIFieldType.File: "APIFile",
        }
        if self.type == APIFieldType.Custom:
            if isinstance(self.custom_type, APIField):
                return self.custom_type.get_converter_name(direction, input_variable, key_only=key_only)
            elif isinstance(self.custom_type, APIModel):
                return build_conv_func(self.custom_type.name, direction, input_variable, key_only=key_only)
            elif isinstance(self.custom_type, str):
                return build_conv_func(self.custom_type, direction, input_variable, key_only=key_only)
            else:
                raise Exception("Invalid custom_type value")
        elif self.type == APIFieldType.Enum:
            assert input_variable is not None
            return "Enum_{dir}({enum_type}, {var})".format(
                    dir=direction.value,
                    enum_type=self.custom_type.__name__,
                    var=input_variable)
        else:
            return build_conv_func(types_map[self.type], direction, input_variable, key_only=key_only)

    def get_type_str(self):
        from .api_model import APIModel

        types_map = {
            APIFieldType.Key: "string",
            APIFieldType.Integer: "number",
            APIFieldType.Float: "number",
            APIFieldType.String: "string",
            APIFieldType.Decimal: "Decimal",
            APIFieldType.Date: "Moment",
            APIFieldType.DateTime: "Moment",
            APIFieldType.Binary: "Uint8Array",
            APIFieldType.Boolean: "boolean",
            APIFieldType.JSON: "any",
            APIFieldType.File: "APIFile",
        }
        if self.type == APIFieldType.Custom:
            if isinstance(self.custom_type, APIField):
                return self.custom_type.get_type_str()
            elif isinstance(self.custom_type, APIModel):
                return self.custom_type.name
            elif isinstance(self.custom_type, str):
                return self.custom_type
            else:
                raise Exception("Invalid custom_type value")
        elif self.type == APIFieldType.Enum:
            return self.custom_type.__name__
        else:
            return types_map[self.type]

    def is_immutable_type(self):
        types_map = {
            APIFieldType.Key: True,
            APIFieldType.Integer: True,
            APIFieldType.Float: True,
            APIFieldType.String: True,
            APIFieldType.Decimal: True,
            APIFieldType.Date: False,
            APIFieldType.DateTime: False,
            APIFieldType.Binary: False,
            APIFieldType.Boolean: True,
            APIFieldType.JSON: False,
            APIFieldType.File: True,

            APIFieldType.Custom: False,
            APIFieldType.Enum: True,
        }
        return types_map[self.type]

    def get_deep_cloner(self, input_variable: str):
        if self.type in [APIFieldType.JSON]:
            return "JSON.parse(JSON.stringify({0}))".format(input_variable)
        elif self.type in [APIFieldType.Date, APIFieldType.DateTime]:
            return "Moment_clone({0})".format(input_variable)
        elif self.type in [APIFieldType.Binary]:
            return "Uint8Array_clone({0})".format(input_variable)
        elif self.is_immutable_type():
            return "{0}".format(input_variable)
        else:
            return "any_deepClone({0}, () => new {1}())".format(input_variable, self.get_type_str())

    def get_cloner(self, input_variable: str):
        if self.type in [APIFieldType.JSON]:
            return "JSON.parse(JSON.stringify({0}))".format(input_variable)
        elif self.type in [APIFieldType.Date, APIFieldType.DateTime]:
            return "Moment_clone({0})".format(input_variable)
        elif self.type in [APIFieldType.Binary]:
            return "Uint8Array_clone({0})".format(input_variable)
        return "{0}".format(input_variable)

    def get_type_str_for_input(self):
        """ Get type name for input parameters """
        return self.get_type_str()

    def __str__(self):
        return "[APIField {0} {1} {2}]".format(self.name, self.type.name, self.custom_type)


class APIListField(APIField):
    def __init__(self,
                 name: Optional[str],
                 subtype: APIFieldType, custom_subtype=None,
                 flags=APIFieldFlags.NoFlag,
                 group: Any = None, groups: List[Any] = None, permissions: List[Any] = None,
                 getter=None, setter=None):
        super().__init__(name, APIFieldType.List, flags=flags, group=group, groups=groups, permissions=permissions,
                         getter=getter, setter=setter)
        self.subtype = subtype
        self.custom_subtype = custom_subtype
        self.item_type: APIField = None

    def initialize(self, mgr: 'APIManager', base_class: Optional['APIModel']):
        super().initialize(mgr, base_class)
        self.item_type = process_type(self.mgr, self.subtype, self.custom_subtype, base_class)
        self.item_type.initialize(mgr, base_class)

    def get_converter_name(self, direction: ConvDirection, input_variable: str = None, key_only=False):
        assert input_variable is not None
        return "List_{dir}{template}({var}, (x) => {conv})".format(
                dir=direction.value,
                template="<{0}>".format(self.item_type.get_type_str()) if direction == ConvDirection.FromServer else "",
                var=input_variable,
                conv=self.item_type.get_converter_name(direction, key_only=key_only, input_variable="x"))

    def get_type_str(self, base_model: 'APIModel' = None):
        return "List<{0}>".format(self.item_type.get_type_str())

    def is_immutable_type(self):
        return False

    def get_deep_cloner(self, input_variable: str):
        return "List_clone({0}, (x) => {1})".format(
                input_variable,
                self.item_type.get_deep_cloner("x"))

    def get_cloner(self, input_variable: str):
        return "List_clone({0}, (x) => {1})".format(
                input_variable,
                self.item_type.get_cloner("x"))

    def get_type_str_for_input(self, base_model: 'APIModel' = None):
        return "List<{0}> | {0}[]".format(self.item_type.get_type_str())

    def __str__(self):
        return "[APIListField {0} Item: {1}]".format(self.name, self.item_type)


class APIMapField(APIField):
    def __init__(self,
                 name: Optional[str],
                 subtype: APIFieldType, custom_subtype=None,
                 keytype=APIFieldType.String, custom_keytype=None,
                 flags=APIFieldFlags.NoFlag,
                 group: Any = None, groups: List[Any] = None, permissions: List[Any] = None,
                 getter=None, setter=None):
        super().__init__(name, APIFieldType.Map, flags=flags, group=group, groups=groups, permissions=permissions,
                         getter=getter, setter=setter)
        self.subtype = subtype
        self.custom_subtype = custom_subtype
        self.keytype = keytype
        self.custom_keytype = custom_keytype
        self.key_type: APIField = None
        self.value_type: APIField = None

    def initialize(self, mgr: 'APIManager', base_class: Optional['APIModel']):
        super().initialize(mgr, base_class)

        self.key_type = process_type(self.mgr, self.keytype, self.custom_keytype, base_class)
        self.value_type = process_type(self.mgr, self.subtype, self.custom_subtype, base_class)
        self.key_type.initialize(mgr, base_class)
        self.value_type.initialize(mgr, base_class)

    def get_converter_name(self, direction: ConvDirection, input_variable: str = None, key_only=False):
        assert input_variable is not None
        return "Map_{dir}{template}({var}, (x) => {key_conv}, (x) => {value_conv})".format(
                dir=direction.value,
                template="<{0},{1}>".format(
                        self.key_type.get_type_str(),
                        self.value_type.get_type_str()) if direction == ConvDirection.FromServer else "",
                var=input_variable,
                key_conv=self.key_type.get_converter_name(direction, key_only=False, input_variable="x"),
                value_conv=self.value_type.get_converter_name(direction, key_only=key_only, input_variable="x"))

    def get_type_str(self):
        return "Map<{0},{1}>".format(self.key_type.get_type_str(), self.value_type.get_type_str())

    def is_immutable_type(self):
        return False

    def get_deep_cloner(self, input_variable: str):
        return "Map_clone({0}, (x) => {1}, (x) => {2})".format(
                input_variable,
                self.key_type.get_deep_cloner("x"),
                self.value_type.get_deep_cloner("x"))

    def get_cloner(self, input_variable: str):
        return "Map_clone({0}, (x) => {1}, (x) => {2})".format(
                input_variable,
                self.key_type.get_cloner("x"),
                self.value_type.get_cloner("x"))

    def __str__(self):
        return "[APIMapField {0} Key: {1} Item: {2}]".format(self.name, self.key_type, self.value_type)


class APIParentRef(APIField):
    def __init__(self, name: Optional[str], type: APIFieldType, custom_type=None, flags=APIFieldFlags.NoFlag):
        super().__init__(name, type, custom_type, flags=flags | APIFieldFlags.Reference)
