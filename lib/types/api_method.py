from typing import List, TYPE_CHECKING

from .api_field import APIField
from .api_class import APIClass

if TYPE_CHECKING:
    from ..api_connection import APIConnection


class APIMethod:
    def __init__(self, apicls: APIClass, met):
        self.apicls = apicls
        self.met = met

    def initialize(self, mgr):
        if self.apicls.has_base_class():
            base_class = self.apicls.get_base_class()
        else:
            base_class = None

        params_fields = self.params
        if params_fields is not None:
            if not isinstance(params_fields, list):
                params_fields = [params_fields]
            for field in params_fields:
                field.initialize(mgr, base_class=base_class)

        response_fields = self.response
        if response_fields is not None:
            if not isinstance(response_fields, list):
                response_fields = [response_fields]

            for field in response_fields:
                field.initialize(mgr, base_class=base_class)

    @property
    def name(self):
        return self.met.__name__

    @property
    def params(self) -> List[APIField]:
        return getattr(self.met, "params", [])

    @property
    def response(self) -> List[APIField]:
        return getattr(self.met, "response", [])

    @property
    def is_public(self):
        return getattr(self.met, "is_public", False)

    @property
    def required_permissions(self):
        return getattr(self.met, "required_permission", [])

    @property
    def fullpath(self):
        return self.apicls.endpoint_path + self.name

    def create_method_instance(self, conn: 'APIConnection'):
        cls = self.apicls.cls
        obj = cls(conn)
        met = getattr(obj, self.name)
        return met

    def __str__(self):
        return "[{0}:{1}]".format(self.apicls.name, self.name)
