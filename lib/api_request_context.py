from typing import Union, List, Any, TYPE_CHECKING, Optional, ContextManager

from abc import abstractmethod
from sqlalchemy.orm import Session

if TYPE_CHECKING:
    from .api_connection import APIConnection


class BaseAPIRequestContext:
    def __init__(self, conn: 'APIConnection') -> None:
        self.conn = conn
        self.packet_num: int = None

    @abstractmethod
    def create_session(self) -> ContextManager:
        pass

    @abstractmethod
    def load_user(self, user_id: Optional[Union[str, int]]):
        pass

    @abstractmethod
    def has_permission(self, permissions: List[Any]):
        pass

    @abstractmethod
    def get_user_data(self):
        pass

    @abstractmethod
    def has_permission_to_lock(self, object_type: str):
        pass

    def has_any_of_role(self, permissions: List[Any]):
        return self.has_permission(permissions)

    def log(self, txt: str):
        self.conn.log_ctx(self, txt)

    def authenticate(self, id: int, name: str):
        self.conn.user_id = id
        self.conn.user_name = name
        self.conn.is_authenticated = True
        self.conn.log("authenticated as #{0} {1}".format(id, name))
        self.load_user(id)

    def deauthenticate(self):
        self.conn.user_id = None
        self.conn.user_name = None
        self.conn.is_authenticated = False
        self.conn.log("deauthenticated")
        self.load_user(None)
