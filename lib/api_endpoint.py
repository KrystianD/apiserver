from .api_connection import APIConnection


class APIEndpoint:
    def __init__(self, conn: APIConnection):
        self.manager = conn.api_manager
        self.conn = conn


"""
class APIDBEndpoint(APIEndpoint):
    def __init__(self, conn: APIConnection):
        super().__init__(conn)

    @method
    @response(APIListField(None, APIFieldType.BaseClass))
    async def get_all(self, ctx: BaseAPIRequestContext):
        cls = type(self)
        DB_CLS = getattr(cls, "DB_CLS")

        objs = ctx.db_session.query(DB_CLS).order_by(DB_CLS.id).all()

        return objs

    @method
    @params(APIField("id", APIFieldType.Key))
    @response(APIField(None, APIFieldType.BaseClass))
    async def get(self, ctx: BaseAPIRequestContext, id: int):
        cls = type(self)
        DB_CLS = getattr(cls, "DB_CLS")

        obj = ctx.db_session.query(DB_CLS).filter_by(id=id).one()

        return obj

    @method
    @params(APIField("obj", APIFieldType.BaseClass))
    @response(APIField(None, APIFieldType.BaseClass))
    async def create(self, ctx: BaseAPIRequestContext, obj):
        obj.id = None
        ctx.db_session.add(obj)
        ctx.db_session.commit()

        return obj

    @method
    @params(APIField("obj", APIFieldType.BaseClass))
    @response(APIField(None, APIFieldType.BaseClass))
    async def update(self, ctx: BaseAPIRequestContext, obj):
        cls = type(self)
        DB_CLS = getattr(cls, "DB_CLS")

        apimodel = APIModel(ctx.conn.api_manager, DB_CLS)

        dbobj = ctx.db_session.query(DB_CLS).filter_by(id=obj.id).one()

        for field in apimodel.fields:
            if hasattr(obj, field.name):
                v = getattr(obj, field.name)
                setattr(dbobj, field.name, v)

        ctx.db_session.commit()

        return obj

    @method
    @params(APIField("id", APIFieldType.Key))
    async def delete(self, ctx: BaseAPIRequestContext, id: int):
        cls = type(self)
        DB_CLS = getattr(cls, "DB_CLS")

        obj = ctx.db_session.query(DB_CLS).filter_by(id=id).one()
        ctx.db_session.delete(obj)
        ctx.db_session.commit()


class APIMongoDBEndpoint(APIEndpoint):
    def __init__(self, conn: APIConnection):
        super().__init__(conn)

    @method
    @response(APIListField(None, APIFieldType.BaseClass))
    async def get_all(self, ctx: BaseAPIRequestContext):
        cls = type(self)
        DB_CLS = getattr(cls, "DB_CLS")

        return DB_CLS.objects()

    @method
    @params(APIField("id", APIFieldType.String))
    @response(APIField(None, APIFieldType.BaseClass))
    async def get(self, ctx: BaseAPIRequestContext, id: str):
        cls = type(self)
        DB_CLS = getattr(cls, "DB_CLS")

        return DB_CLS.objects.get(id=id)

    @method
    @params(APIField("obj", APIFieldType.BaseClass))
    @response(APIField(None, APIFieldType.BaseClass))
    async def create(self, ctx: BaseAPIRequestContext, obj):
        cls = type(self)
        # DB_CLS = getattr(cls, "DB_CLS")

        obj.save(force_insert=True)
        return obj

    @method
    @params(APIField("obj", APIFieldType.BaseClass))
    @response(APIField(None, APIFieldType.BaseClass))
    async def update(self, ctx: BaseAPIRequestContext, obj):
        cls = type(self)
        # DB_CLS = getattr(cls, "DB_CLS")

        obj.save()
        return obj

    @method
    @params(APIField("id", APIFieldType.String))
    async def delete(self, ctx: BaseAPIRequestContext, id: str):
        cls = type(self)
        DB_CLS = getattr(cls, "DB_CLS")

        DB_CLS.objects.get(id=id).delete()
"""
