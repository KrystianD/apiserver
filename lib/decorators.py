import re


def endpoint(path):
    def wrapper(cls):
        cls.is_endpoint = True
        nonlocal path
        path = path.strip("/")
        if len(path) > 0:
            path = "/" + path + "/"
        else:
            path = "/"

        cls.endpoint_path = path

        return cls

    return wrapper


def method(f):
    f.is_endpoint = True
    return f


def _process_arg(arg):
    if isinstance(arg, tuple):
        arg = list(arg)

    if isinstance(arg, list):
        if len(arg) == 1:
            arg = arg[0]

    return arg


def params(*params):
    def wrapper(f):
        f.params = _process_arg(params)
        return f

    return wrapper


def response(*response):
    def wrapper(f):
        f.response = _process_arg(response)
        return f

    return wrapper


def customparams(f):
    f.params = None
    return f


def public(f):
    f.is_public = True
    return f


def required_role(*args):
    return required_permission(*args)


def required_permission(*perms):
    def wrapper(cls):
        cls.required_permission = list(perms)
        return cls

    return wrapper


def apimodel(f):
    f._apimodel = True
    f._apimodelinternal = False
    return f

def internalapimodel(f):
    f._apimodel = True
    f._apimodelinternal = True
    return f


def apimodelflags(flags):
    def wrapper(cls):
        cls._apimodelflags = flags
        return cls

    return wrapper
