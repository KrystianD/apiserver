from typing import List, Dict, TYPE_CHECKING, Optional, Union
import datetime
import logging

from enum import Enum
import shortuuid

from .types import APIField, APIFieldType
from .decorators import internalapimodel

if TYPE_CHECKING:
    from .api_connection import APIConnection
    from .api_manager import APIManager


class APIInvalidLockException(Exception):
    pass


class APILockKey:
    def __init__(self, object_type: Union[str, Enum], object_id: Union[str, int]):
        if isinstance(object_type, Enum):
            object_type = object_type.name

        if isinstance(object_id, int):
            object_id = str(object_id)

        self.object_type: str = object_type
        self.object_id: str = object_id

    def __hash__(self):
        return hash((self.object_type, self.object_id))

    def __eq__(self, other):
        return (self.object_type, self.object_id) == (other.object_type, other.object_id)

    def __ne__(self, other):
        return not (self == other)


@internalapimodel
class APILock:
    def __init__(self):
        self.id: str = None
        self.object_type: str = None
        self.object_id: str = None
        self.user_id: int = None
        self.conn: APIConnection = None
        self.acquire_date: datetime.datetime = None

    FIELDS = [
        APIField("id", APIFieldType.String),
        APIField("object_type", APIFieldType.String),
        APIField("object_id", APIFieldType.Key),
        APIField("user_id", APIFieldType.Key),
    ]

    @property
    def key(self):
        return APILockKey(self.object_type, self.object_id)

    def __str__(self):
        return "[Lock #{0} {1} {2} user: {3}]".format(self.id, self.object_type, self.object_id, self.user_id)


class APILockManager:
    def __init__(self, manager: 'APIManager') -> None:
        self.manager = manager
        self.locks: List[APILock] = []
        self.locks_key_map: Dict[APILockKey, APILock] = {}
        self.locks_id_map: Dict[str, APILock] = {}
        self.locks_type_map: Dict[str, List[APILock]] = {}
        self.locks_conn_map: Dict[APIConnection, List[APILock]] = {}

        self.connection_watchers: Dict[APIConnection, List[APILockKey]] = {}

    def acquire_lock(self, object_type: Union[str, Enum], object_id: int, conn: 'APIConnection') -> Optional[APILock]:
        # TODO: protect if multiple workers

        key = APILockKey(object_type, object_id)

        if key in self.locks_key_map:
            return None

        lock = APILock()
        lock.id = shortuuid.uuid()
        lock.object_type = key.object_type
        lock.object_id = key.object_id
        lock.user_id = conn.user_id
        lock.conn = conn
        lock.acquire_date = datetime.datetime.utcnow()

        if lock.conn not in self.locks_conn_map:
            self.locks_conn_map[lock.conn] = []

        if lock.object_type not in self.locks_type_map:
            self.locks_type_map[lock.object_type] = []

        self.locks.append(lock)
        self.locks_key_map[lock.key] = lock
        self.locks_id_map[lock.id] = lock
        self.locks_type_map[lock.object_type].append(lock)
        self.locks_conn_map[lock.conn].append(lock)

        logging.info("lock acquired: {0}".format(lock))

        for conn in self.manager.get_connection():
            if lock.object_type in conn.watched_locks:
                conn.send_notification("lock", {
                    "type": "acquired",
                    "lock": self.manager.serialize_model(lock),
                })

        return lock

    def release_lock(self, id: str, conn: 'APIConnection'):
        # TODO: protect if multiple workers

        if id not in self.locks_id_map:
            return

        lock = self.locks_id_map[id]

        assert lock.user_id == conn.user_id

        del self.locks_id_map[id]
        del self.locks_key_map[lock.key]
        self.locks.remove(lock)
        self.locks_conn_map[lock.conn].remove(lock)
        self.locks_type_map[lock.object_type].remove(lock)
        self._notify_lock_release(lock)

        logging.info("lock released: {0}".format(lock))

    def validate_lock(self, lock_id: str, object_type: Union[str, Enum], object_id: int, conn: 'APIConnection'):
        key = APILockKey(object_type, object_id)

        if key in self.locks_key_map:
            lock = self.locks_key_map[key]
            if lock.id != lock_id:
                raise APIInvalidLockException("Invalid lock id")
            if lock.user_id != conn.user_id:
                raise APIInvalidLockException("Invalid lock user")
        else:
            raise APIInvalidLockException("Invalid lock id")

    def release_conn(self, conn: 'APIConnection'):
        # TODO: protect if multiple workers

        if conn not in self.locks_conn_map:
            return

        locks = self.locks_conn_map[conn]
        for lock in locks:
            del self.locks_id_map[lock.id]
            del self.locks_key_map[lock.key]
            self.locks_type_map[lock.object_type].remove(lock)

            self._notify_lock_release(lock)

            logging.info("lock released: {0}".format(lock))

        del self.locks_conn_map[conn]

    def _notify_lock_release(self, lock: APILock):
        for conn in self.manager.get_connection():
            if lock.object_type in conn.watched_locks:
                conn.send_notification("lock", {
                    "type": "released",
                    "lock_id": lock.id,
                })

    def get_locks_by_object_type(self, object_type: str) -> List[APILock]:
        return self.locks_type_map.get(object_type, [])

    def get_locks_by_conn(self, conn: 'APIConnection') -> List[APILock]:
        return self.locks_conn_map[conn]
