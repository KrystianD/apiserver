import asyncio
import json
import logging
import traceback
from typing import Union, List, Any, Dict, Set, Tuple

from enum import Enum
import copy
import aiohttp
import shortuuid
from aiohttp.web_request import Request
from aiohttp.web_ws import WebSocketResponse

from lib.apiserver.lib.types.exceptions import APIServerUserException
from .types import APIMethod
from .api_request_context import BaseAPIRequestContext
from .utils import get_client_ip
from .api_manager import APIManager

log = logging.getLogger("API")


class APIBaseException(Exception):
    def __init__(self, packet: dict, *args):
        super().__init__(*args)
        self.packet = packet


class APIInvalidCommandException(APIBaseException):
    def __init__(self, packet: dict, *args):
        super().__init__(packet, *args)


class APINotAuthenticatedException(APIBaseException):
    def __init__(self, packet: dict, *args):
        super().__init__(packet, *args)


class APINoPermissionsException(Exception):
    def __init__(self, method: APIMethod, packet: dict, required_permissions: List[Any], *args):
        msg = "{}/{}, {}, {}".format(method.apicls.name, method.name, packet, required_permissions)
        super().__init__(msg, *args)
        self.required_permissions = required_permissions


def clean_params(params: Dict[str, Any]):
    for key, value in params.items():
        if value is not None:
            if isinstance(value, str):
                if len(value) > 200:
                    params[key] = "xxx"
                elif len(value) > 40:
                    params[key] = params[key][:37] + "..."

        if key == "password":
            params[key] = "xxx"

        if isinstance(value, dict):
            clean_params(value)

    return params


class APIConnection:
    def __init__(self, api_manager: APIManager, request: Request, websocket: WebSocketResponse):
        self.connection_session_id = shortuuid.uuid()
        self.api_manager = api_manager
        self.websocket = websocket
        self.ip = get_client_ip(request, trusted_proxies=('127.0.0.1', '172.17.0.1'))
        self.user_id = None  # type: Union[str,int]
        self.user_name = None  # type: str
        self.is_authenticated = False
        self.watched_locks: Set[str] = set()
        self.change_subscriptions: Set[Tuple[str, str]] = set()

    def _get_log_line(self, txt: str, num: int = None):
        user_str = "unknown          "
        if self.user_name is not None and self.user_id is not None:
            user_str = "{:<17s}".format("#{0} {1}".format(self.user_id, self.user_name)[:17])

        num_str = ""
        if num is not None:
            num_str = "[{0:>3s}] ".format("*{0}".format(num))

        return "[{ip:>15s}] [{user}] {num}{txt}".format(ip=self.ip, user=user_str, num=num_str, txt=txt)

    def log(self, x):
        log.info(self._get_log_line(x))

    def log_ctx(self, ctx: BaseAPIRequestContext, x):
        log.info(self._get_log_line(x, ctx.packet_num))

    def warning(self, x):
        log.warning(self._get_log_line(x))

    async def run(self):
        info_packet = {
            "type": "info",
            "server_revision": self.api_manager.revision,
            "server_session": self.api_manager.server_session_id,
        }
        asyncio.get_event_loop().create_task(self.websocket.send_json(info_packet))

        async for packet in self.websocket:
            if packet.type == aiohttp.WSMsgType.TEXT:
                packet = json.loads(packet.data)

                if packet["cmd"] == "ping":
                    continue

                self.process_packet(packet)

    def process_packet(self, packet):
        num, cmd, params = packet["num"], packet["cmd"], packet.get("params", {})

        ctx = self.api_manager.context_cls(self)
        ctx.packet_num = num

        cparams = clean_params(copy.deepcopy(params))
        ctx.log("{0} {1}".format(cmd, cparams))

        # noinspection PyBroadException
        try:
            api_method = self.api_manager.create_endpoint_instance(cmd, self)
            if api_method is None:
                raise APIInvalidCommandException(cparams)

            asyncio.get_event_loop().create_task(self.run_method(num, ctx, api_method, params))
        except Exception as e:
            self._process_exception(e, num, ctx)

    async def run_method(self, num, ctx: BaseAPIRequestContext, method: APIMethod, params):
        cparams = clean_params(copy.deepcopy(params))

        # noinspection PyBroadException
        with ctx.create_session():
            try:
                if self.is_authenticated:
                    ctx.load_user(self.user_id)

                    if self.api_manager.sentry_client is not None:
                        self.api_manager.sentry_client.user_context(ctx.get_user_data())

                if not method.is_public:
                    if not self.is_authenticated:
                        raise APINotAuthenticatedException(cparams)

                    p = method.apicls.required_permissions
                    if len(p) > 0:
                        if not ctx.has_permission(p):
                            raise APINoPermissionsException(method, cparams, p)

                    p = method.required_permissions
                    if len(p) > 0:
                        if not ctx.has_permission(p):
                            raise APINoPermissionsException(method, cparams, p)

                method_func = method.create_method_instance(self)
                params = self.api_manager.process_input_params(method, params)
                data = await method_func(ctx, **params)
                data = self.api_manager.process_output_value(ctx, method, data)
                self.send({
                    "type": "response",
                    "num": num,
                    "response": data,
                    "result": "ok",
                })
            except Exception as e:
                self._process_exception(e, num, ctx)

    def _process_exception(self, exc, num, ctx):
        if not isinstance(exc, (PermissionError, APINotAuthenticatedException, APINoPermissionsException)):
            if self.api_manager.sentry_client is not None:
                self.api_manager.sentry_client.captureException()

            traceback.print_exc()

        data = {
            "type": "response",
            "num": num,
            "result": "error",
        }

        if isinstance(exc, APIServerUserException):
            data["result"] = "user-error"
            data["message"] = exc.message

        self.send(data)

    def send_notification(self, type: str, json_data):
        self.send({
            "type": "notification",
            "ntype": type,
            "payload": json_data,
        })

    def add_locks_watcher(self, object_type: Union[str, Enum]):
        if isinstance(object_type, Enum):
            object_type = object_type.name
        self.watched_locks.add(object_type)

    def add_change_subscription(self, key: 'APIChangeKey'):
        self.api_manager.changes_manager.subscribe(self, key)

    def send(self, json_data):
        asyncio.get_event_loop().create_task(self.websocket.send_json(json_data))
