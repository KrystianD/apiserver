import json
from typing import List

import os

from .types.api_class import APIClass
from .types.api_field import ConvDirection
from .api_manager import APIManager
from .types import APIMethod, APIModel, APIFieldType, APIField, APIListField, APIMapField, APIFieldFlags
from .utils import path_to_camelcase

script_dir = os.path.dirname(os.path.realpath(__file__))


def get_asset(name):
    with open(os.path.join(script_dir, "ts", name), "rt") as f:
        return f.read()


def save_file(path, content):
    with open(path, "wt") as f:
        f.write(content)


class TSGenerator:
    def __init__(self,
                 api_manager: APIManager,
                 api_files_path: str,
                 user_classes_path: str,
                 force):
        self.api_manager = api_manager
        self.api_files_path = api_files_path
        self.user_classes_path = user_classes_path
        self.processed_modules = {}
        self.processed_classes = {}

        self.force = force

        self.from_converters = {}
        self.usertypes = {}
        self.out_enums = ""
        self.out_user_imports = ""
        self.out_class_from_conv = ""
        self.out_from_converters = ""
        self.out_cmds = ""
        self.out_classes = ""
        self.out_export = "export * from './api.models.gen';\n"
        self.out_dataobj = ""
        self.out_cloners = ""
        self.out_endpoints = ""

        self.classes = []

    def generate(self):
        self.api_manager.initialize()

        for model_cls in self.api_manager.models.values():
            self.process_model(model_cls)

        for ep in self.api_manager.endpoints.values():
            for met in ep.values():
                self.emit_cmd(met)

        for ep in self.api_manager.endpoint_classes:
            self.emit_ep(ep)

        self.save()

    def emit_class_from_conv_line(self, line=""):
        self.out_class_from_conv += line + "\n"

    def emit_from_conv_line(self, line=""):
        self.out_from_converters += line + "\n"

    def emit_enum_type(self, enum_cls):
        enum_name = enum_cls.__name__

        if enum_name in self.usertypes:
            return
        self.usertypes[enum_name] = True

        self.out_enums += "export class {0} {{\n".format(enum_name)
        self.out_from_converters = "import {{ {0} }} from './api.models.gen';\n".format(enum_name) + \
                                   self.out_from_converters

        conv_str = ""
        map_str = ""
        list_all = []
        for i, m in enumerate(enum_cls):
            self.out_enums += '\tpublic static {0} = new {1}("{0}", "{2}", {3});\n'.format(m.name, enum_name, m.value,
                                                                                           i)

            conv_str += """
      case "{enum_item}":
      case {enum_name}.{enum_item}:
        return {enum_name}.{enum_item};""".format(enum_name=enum_name, enum_item=m.name)

            list_all.append("\n    {enum_name}.{enum_item}".format(enum_name=enum_name, enum_item=m.name))

        self.out_enums += """
  private static members: {enum_name}[] = [{list_all}
  ];
  private static map = {enum_name}.members.map(x => <any>{{ value: x, label: x.label }});
  private static mapwnull = [<any>{{ value: null, label: "not set" }}].concat({enum_name}.map);

  private index: number;
  private name: string;
  private label: string;

  public static fromString(x: string | {enum_name}) {{
    if (x == null)
      return null;
    switch (x) {{{conv_str}
      default:
        throw new Error("unknown enum value: " + x);
    }}
  }}

  public static getMembers(): {enum_name}[] {{
    return {enum_name}.members;
  }}

  public static getValueLabelMap(include_null = false): any[] {{
    return include_null ? {enum_name}.mapwnull : {enum_name}.map;
  }}

  private constructor(name, label, index) {{
    this.index = index;
    this.name = name;
    this.label = label;
  }}

  public getIndex() {{ return this.index; }}
  public getName() {{ return this.name; }}
  public getLabel() {{ return this.label; }}
  public toString() {{ return this.name; }}
  public valueOf() {{ return this.name; }}
}}
""".format(enum_name=enum_name, conv_str=conv_str, map_str=map_str, list_all=",".join(list_all))

    def create_class(self, name: str, fields: List[APIField], user_class_name=None):
        user_class_name = user_class_name or name

        content = ""
        content += "export class {0} implements ICloneable {{".format(name)

        for field in fields:
            self.process_type(field)
            content += "\n\tpublic {0}: {1} = null;".format(field.name, field.get_type_str())

        content += "\n"
        content += "\n"
        content += "\t// monkey-patched in api.service.gen.ts\n"
        content += "\tpublic clone(): {0} {{ return null; }}\n".format(user_class_name)
        content += "\tpublic cloneTo(obj) { }\n"
        content += "\tpublic deepClone(): {0} {{ return null; }}\n".format(user_class_name)
        content += "\tpublic deepCloneTo(obj) { }\n"

        # self.out_cloners += "(BaseModels.{0}.prototype as any).clone = function () {{\n".format(name)
        # self.out_cloners += "\tlet obj = new {0}();\n".format(user_class_name)
        # self.out_cloners += "\tthis.cloneTo(obj);\n".format(user_class_name)
        # self.out_cloners += "\treturn obj;\n"
        # self.out_cloners += "};\n"

        self.out_cloners += "(BaseModels.{0}.prototype as any).deepClone = function () {{\n".format(name)
        self.out_cloners += "\tlet obj = new {0}();\n".format(user_class_name)
        self.out_cloners += "\tthis.deepCloneTo(obj);\n".format(user_class_name)
        self.out_cloners += "\treturn obj;\n"
        self.out_cloners += "};\n"

        # self.out_cloners += "(BaseModels.{0}.prototype as any).cloneTo = function (obj) {{\n".format(name)
        # for field in fields:
        #     conv = field.get_cloner("this.{0}".format(field.name))
        #     self.out_cloners += "\t\tobj.{0} = {1};\n".format(field.name, conv)
        # self.out_cloners += "};\n"

        self.out_cloners += "(BaseModels.{0}.prototype as any).deepCloneTo = function (obj) {{\n".format(name)
        for field in fields:
            conv = field.get_deep_cloner("this.{0}".format(field.name))
            self.out_cloners += "\t\tobj.{0} = {1};\n".format(field.name, conv)
        self.out_cloners += "};\n"

        content += "}"

        self.out_classes += content + "\n"
        return content

    def emit_class_converters(self, name,
                              fields: List[APIField],
                              api_model: APIModel = None,
                              import_filename: str = None,
                              import_from_base: bool = False,
                              emit_from_server: bool = True,
                              emit_to_server: bool = True):
        if name in self.from_converters:
            return
        self.from_converters[name] = True

        self.emit_class_from_conv_line()
        if import_filename:
            self.emit_class_from_conv_line(
                "import {{ {0} }} from './{1}';".format(name, self.get_user_model_path_from_api_dir(import_filename)))
        elif import_from_base:
            self.emit_class_from_conv_line("import {{ {0} }} from './api.models.gen';".format(name))

        if emit_from_server:
            # JSON to class
            self.emit_class_from_conv_line("export function {name}_fromServer(dict) {{".format(name=name))
            self.emit_class_from_conv_line("\tif (dict === null) return null;")
            self.emit_class_from_conv_line("\tif (dict === undefined) return undefined;")
            self.emit_class_from_conv_line("\tlet obj = new {0}();".format(name))

            for field in fields:
                self.emit_class_from_conv_line(
                    '\tobj.{name} = {converter_func};'.format(
                        name=field.name,
                        converter_func=field.get_converter_name(ConvDirection.FromServer,
                                                                'dict["{0}"]'.format(field.name))))

            self.emit_class_from_conv_line("\treturn obj;")
            self.emit_class_from_conv_line("}")

        if emit_to_server:
            # class to JSON
            self.emit_class_from_conv_line("export async function {name}_toServer(obj) {{".format(name=name))
            self.emit_class_from_conv_line("\tif (obj === null) return null;")
            self.emit_class_from_conv_line("\tif (obj === undefined) return undefined;")
            self.emit_class_from_conv_line("\tlet dict = {};")

            for field in fields:
                if APIFieldFlags.ToClientOnly in field.flags:
                    continue

                key_only = False
                if APIFieldFlags.Reference in field.flags:
                    key_only = True

                self.emit_class_from_conv_line(
                    '\tdict["{0}"] = await {converter_func};'.format(
                        field.name,
                        converter_func=field.get_converter_name(
                            ConvDirection.ToServer,
                            "obj.{0}".format(field.name),
                            key_only=key_only)))

            self.emit_class_from_conv_line("\treturn dict;")
            self.emit_class_from_conv_line("}")

            # class to JSON (key only)
            if api_model is not None:
                key_field = api_model.key_field
                if key_field is not None:
                    self.emit_class_from_conv_line(
                        "export async function {name}_key_toServer(obj) {{".format(name=name))
                    self.emit_class_from_conv_line("\tif (obj === null) return null;")
                    self.emit_class_from_conv_line("\tif (obj === undefined) return undefined;")
                    self.emit_class_from_conv_line("\tlet dict = {};")

                    self.emit_class_from_conv_line(
                        '\tdict["{0}"] = await {converter_func};'.format(
                            key_field.name,
                            converter_func=key_field.get_converter_name(ConvDirection.ToServer,
                                                                        "obj.{0}".format(key_field.name))))

                    self.emit_class_from_conv_line("\treturn dict;")
                    self.emit_class_from_conv_line("}")

    def emit_request_class_converter(self, name, fields: List[APIField]):
        self.create_class(name, fields)
        self.emit_class_converters(name, fields, import_from_base=True, emit_from_server=False, emit_to_server=True)

    def emit_response_class_converter(self, name, fields: List[APIField]):
        self.create_class(name, fields)
        self.emit_class_converters(name, fields, import_from_base=True, emit_from_server=True, emit_to_server=False)

    def get_user_model_path_from_api_dir(self, filename):
        api_models_path = os.path.realpath(self.api_files_path)
        user_model_path = os.path.join(os.path.realpath(self.user_classes_path), filename)
        user_model_path_rel = os.path.relpath(user_model_path, api_models_path)
        return user_model_path_rel

    def create_user_class(self, model: APIModel):
        name = model.name
        filename = model.filename

        api_models_path = os.path.join(os.path.realpath(self.api_files_path), "api.models.gen")
        user_model_path = os.path.join(os.path.realpath(self.user_classes_path), filename)
        api_models_path_rel = os.path.relpath(api_models_path, os.path.dirname(user_model_path))
        user_model_path_rel = self.get_user_model_path_from_api_dir(filename)

        self.out_user_imports += "import {{ {0} }} from './{1}';\n".format(name, user_model_path_rel)
        self.out_export += "export * from './{1}';\n".format(name, user_model_path_rel)

        content = ""
        content += "import {{ Base{0} }} from '{1}';\n\n".format(name, api_models_path_rel)
        content += "export class {0} extends Base{0} {{\n".format(name)
        content += "}\n"

        api_path = os.path.join(self.user_classes_path, "{0}.ts".format(filename))
        if self.force or not os.path.exists(api_path):
            with open(api_path, "wt") as f:
                f.write(content)

    def create_params_list(self, method_name: str, fields: List[APIField]):
        param, data_dict = "", ""

        if fields is None:
            fields = []

        if fields is None:
            return "", "\t\t\tlet req = {};"

        if isinstance(fields, list):
            if len(fields) == 0:
                return "", "\t\t\tlet req = {};"

            params_class_name = method_name + "Params"
            params_class_name = params_class_name[0].upper() + params_class_name[1:]

            self.emit_request_class_converter(params_class_name, fields)

            param = "params: {type}".format(type=params_class_name)
            data_dict = "\t\t\tlet req = await {type}_toServer(params);".format(type=params_class_name)

        elif isinstance(fields, APIField):
            f = fields

            type = f.get_type_str_for_input()
            conv = f.get_converter_name(ConvDirection.ToServer, f.name)

            param = "{name}: {type}".format(name=f.name, type=type)
            data_dict = "\t\t\tlet req = {{ {name}: await {conv} }};".format(name=f.name, conv=conv)

        return param, data_dict

    def process_model(self, model: APIModel):
        # if cls.__name__ i n self.processed_classes:
        #    return
        # self.processed_classes[cls.__name__] = True

        if not model.is_internal:
            self.create_class("Base{0}".format(model.name), model.fields, user_class_name=model.name)
            self.emit_class_converters(model.name, model.fields, api_model=model, import_filename=model.filename)
            self.create_user_class(model)
        else:
            self.emit_class_converters(model.name, model.fields, api_model=model, import_from_base=True)
            self.create_class("{0}".format(model.name), model.fields)

    def register_data(self, obj):
        model = APIModel(self.api_manager, type(obj))

        objdata = self.api_manager.serialize_model(obj)
        for f in model.fields:
            data = json.dumps(objdata[f.name], indent=2)
            self.out_dataobj += "\tpublic static {0} = {1};\n" \
                .format(f.name,
                        f.get_converter_name(ConvDirection.FromServer, data))

    def emit_ep(self, ep: APIClass):

        commands = ""

        for m in ep.get_methods():
            commands += self.generate_cmd_new(m)

        self.out_endpoints += """
export class {name} {{
  constructor(private api: BaseAPIService) {{ }}
{commands}
}}
""".format(name=ep.name, commands=commands)

        self.out_cmds += "\tpublic {name} = new {name}(this);\n".format(name=ep.name)

        print(ep.name)
        pass

    def generate_cmd_new(self, api_method: APIMethod):
        path = api_method.apicls.endpoint_path + api_method.name
        method_name = path_to_camelcase(path)
        mn = path_to_camelcase(api_method.name)

        params = api_method.params
        response_fields = api_method.response

        # self.out_cmds += "\n"

        return_type = None

        arg_list, data_dict = self.create_params_list(method_name, params)

        if response_fields is None:
            pass

        if isinstance(response_fields, APIField):
            if response_fields.name is not None:
                response_fields = [response_fields]
            else:
                return_type = response_fields

        if isinstance(response_fields, list):
            if len(response_fields) > 0:
                response_class_name = method_name + "Response"
                response_class_name = response_class_name[0].upper() + response_class_name[1:]

                self.emit_response_class_converter(response_class_name, response_fields)
                return_type = APIField(None, APIFieldType.Custom, response_class_name)

        return_fulltype_str = ": Rx.Observable<void>"
        return_map_str = ""

        if return_type is not None:
            return_fulltype_str = ": Rx.Observable<{return_type}>".format(
                return_type=return_type.get_type_str())

            return_map_str = ".map(resp => {return_conv})" \
                .format(return_conv=return_type.get_converter_name(ConvDirection.FromServer, "resp"))

            self.process_type(return_type)

        return """
  public {method_name}({arg_list}){return_fulltype_str} {{
    async function conv() {{
{data_dict}
      return req;
    }}
    return Rx.Observable.fromPromise(conv())
      .flatMap(req => this.api.cmd("{path}", req))
      {return_map_str};
  }}""".format(
            method_name=mn,
            path=path,
            return_fulltype_str=return_fulltype_str,
            return_map_str=return_map_str,
            arg_list=arg_list,
            data_dict=data_dict)

    def emit_cmd(self, tsmet: APIMethod):
        tscls = tsmet.apicls
        path = tscls.endpoint_path + tsmet.name
        method_name = path_to_camelcase(path)

        params = tsmet.params
        response_fields = tsmet.response

        self.out_cmds += "\n"

        return_type = None

        def create_params_listold(method_name: str, fields: List[APIField]):
            param, data_dict = "", ""

            if fields is None:
                fields = []

            if fields is None:
                return "", "\t\t\tlet req = {};"

            if isinstance(fields, list):
                if len(fields) == 0:
                    return "", "\t\t\tlet req = {};"

                params_class_name = method_name + "Params"
                params_class_name = params_class_name[0].upper() + params_class_name[1:]

                param = "params: {type}".format(type=params_class_name)
                data_dict = "\t\t\tlet req = await {type}_toServer(params);".format(type=params_class_name)

            elif isinstance(fields, APIField):
                f = fields

                type = f.get_type_str_for_input()
                conv = f.get_converter_name(ConvDirection.ToServer, f.name)

                param = "{name}: {type}".format(name=f.name, type=type)
                data_dict = "\t\t\tlet req = {{ {name}: await {conv} }};".format(name=f.name, conv=conv)

            return param, data_dict

        arg_list, data_dict = create_params_listold(method_name, params)

        if response_fields is None:
            pass

        if isinstance(response_fields, APIField):
            if response_fields.name is not None:
                response_fields = [response_fields]
            else:
                return_type = response_fields

        if isinstance(response_fields, list):
            if len(response_fields) > 0:
                response_class_name = method_name + "Response"
                response_class_name = response_class_name[0].upper() + response_class_name[1:]

                #self.emit_response_class_converter(response_class_name, response_fields)
                return_type = APIField(None, APIFieldType.Custom, response_class_name)

        return_fulltype_str = ": Rx.Observable<void>"
        return_map_str = ""

        if return_type is not None:
            return_fulltype_str = ": Rx.Observable<{return_type}>".format(
                return_type=return_type.get_type_str())

            return_map_str = ".map(resp => {return_conv})" \
                .format(return_conv=return_type.get_converter_name(ConvDirection.FromServer, "resp"))

            self.process_type(return_type)

        self.out_cmds += """
  public {method_name}({arg_list}){return_fulltype_str} {{
    async function conv() {{
{data_dict}
      return req;
    }}
    return Rx.Observable.fromPromise(conv())
      .flatMap(req => this.cmd("{path}", req))
      {return_map_str};
  }}""".format(
            method_name=method_name,
            path=path,
            return_fulltype_str=return_fulltype_str,
            return_map_str=return_map_str,
            arg_list=arg_list,
            data_dict=data_dict)

    def save(self):
        api_models_gen_file = """// AUTOGENERATED, DO NOT TOUCH
import Decimal from 'decimal.js';
import { Moment } from 'moment';
import { List } from 'kdlib';

export interface ICloneable {
  clone();
  cloneTo(obj: any);
  deepClone();
  deepCloneTo(obj: any);
}

export interface APIFile {
  prototype: File;
  new (parts: (ArrayBuffer | ArrayBufferView | Blob | string)[], filename: string, properties?: FilePropertyBag): File;
}
"""
        api_models_gen_file += "\n"
        api_models_gen_file += self.out_user_imports + "\n"
        api_models_gen_file += self.out_enums
        api_models_gen_file += self.out_classes

        self.out_export += "export { APIData } from './api.service.gen';\n"

        api_models_gen_file = api_models_gen_file.replace("\t", "  ")
        self.out_export = self.out_export.replace("\t", "  ")

        api_service_ts = get_asset("api.service.gen.ts") \
            .replace("%TYPES_CONVERTERS%", self.out_from_converters) \
            .replace("%CLASS_CONVERTERS%", self.out_class_from_conv) \
            .replace("%CLONERS%", self.out_cloners) \
            .replace("%CMDS%", self.out_cmds) \
            .replace("%ENDPOINTS%", self.out_endpoints) \
            .replace("%DATA_FIELDS%", self.out_dataobj) \
            .replace("\t", "  ")

        api_path = os.path.join(self.api_files_path, "api.service.gen.ts")
        save_file(api_path, api_service_ts)

        api_path = os.path.join(self.api_files_path, "api.service.ts")
        if self.force or not os.path.exists(api_path):
            save_file(api_path, get_asset("api.service.ts"))

        api_path = os.path.join(self.api_files_path, "api.models.gen.ts")
        save_file(api_path, api_models_gen_file)

        api_path = os.path.join(self.api_files_path, "api.export.gen.ts")
        save_file(api_path, self.out_export)

        api_path = os.path.join(self.api_files_path, "api-guard.service.gen.ts")
        api_service_ts = get_asset("api-guard.service.ts")
        save_file(api_path, api_service_ts)

    def process_type(self, field: APIField):
        if field.type == APIFieldType.Enum:
            self.emit_enum_type(field.custom_type)

        if isinstance(field, APIListField):
            self.process_type(field.item_type)

        elif isinstance(field, APIMapField):
            self.process_type(field.key_type)
            self.process_type(field.value_type)
