import datetime
import base64
from typing import Dict


class APIFile:
    def __init__(self):
        self.name: str = None
        self.type: str = None
        self.modification_date: datetime.datetime = None
        self.data: bytes = None

    @staticmethod
    def from_dict(data: Dict[str, str]):
        obj = APIFile()
        obj.name = data["name"]
        obj.type = data["type"]
        obj.modification_date = datetime.datetime.strptime(data["moddate"].rstrip("Z"), "%Y-%m-%dT%H:%M:%S.%f")
        obj.data = base64.b64decode(data["data"], validate=True)
        return obj
