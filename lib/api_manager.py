import glob
import inspect
import logging
import traceback
import sys
from typing import Dict, List, Type, TypeVar, TYPE_CHECKING, Any
import imp
import os
from asyncio import CancelledError

import raven
import shortuuid
from aiohttp.web_request import Request
from hashids import Hashids

from .api_serialization_context import APISerializationContext, APIInternalSerializationContext
from .serializer import Serializer
from .api_lock_manager import APILockManager, APILock, APIInvalidLockException
from .api_request_context import BaseAPIRequestContext
from .types import APIClass, APIMethod, APIModel, APIField, APIListField

if TYPE_CHECKING:
    from .api_connection import APIConnection

T = TypeVar("T")

ALPHABET = 'ABCDEFGHKLMNOPRSTUWXYZ1234567890'


def load_module(file_path):
    with open(file_path, "rb") as f:
        name = os.path.splitext(file_path)
        name = os.path.basename(name[0])

        for m in sys.modules.values():
            if getattr(m, "__file__", "") == os.path.realpath(file_path):
                return m

        return imp.load_module(name, f, file_path, ('.py', 'U', 1))


class APIManager:
    instance: 'APIManager' = None
    revision: str = None

    def __init__(self, secret_key: str) -> None:
        from .mgmt_ep import MgmtEndpoint
        from .api_connection import APIConnection
        from .api_changes_manager import APIChangesManager

        self.server_session_id = shortuuid.uuid()
        self.endpoint_classes: List[APIClass] = []
        self.endpoints: Dict[str, Dict[str, APIMethod]] = {}
        self.models: Dict[str, APIModel] = {}
        self.context_cls: Type[BaseAPIRequestContext] = BaseAPIRequestContext
        self.sentry_client: raven.Client = None
        self.secret_key = secret_key
        self.hashids = Hashids(salt=secret_key, min_length=8, alphabet=ALPHABET)
        self.hashids_ns = {}
        self.connections: List[APIConnection] = []
        self.lock_manager = APILockManager(self)
        self.changes_manager = APIChangesManager(self)
        self.serializer = Serializer(self)

        # noinspection PyTypeChecker
        self.register_endpoint(MgmtEndpoint)
        # noinspection PyTypeChecker
        self.register_model(APILock)

    def get_connection(self) -> List['APIConnection']:
        return self.connections

    def register_context(self, context_cls):
        self.context_cls = context_cls

    def register_sentry(self, sentry_client: raven.Client):
        self.sentry_client = sentry_client

    def register_endpoints_in_path(self, path: str):
        files = [path] + glob.glob(path + "/*.py") + glob.glob(path + "/**/*.py")
        for file_path in files:
            if not os.path.isfile(file_path):
                continue
            try:
                m = load_module(file_path)
                self.register_endpoints_in_module(m)

            except Exception as inst:
                traceback.print_exc()
                logging.error("Unable to load {0} error: {1}".format(file_path, inst))
                raise

    def register_models_in_module(self, mod):
        for member in inspect.getmembers(mod):
            obj = member[1]
            if inspect.isclass(obj) and hasattr(obj, "_apimodel"):
                self.register_model(obj)

    def register_endpoints_in_module(self, mod):
        for member in inspect.getmembers(mod):
            obj = member[1]
            if inspect.isclass(obj) and hasattr(obj, "is_endpoint"):
                self.register_endpoint(obj)

    def register_models_in_path(self, path: str):
        files = [path] + glob.glob(path + "/*.py") + glob.glob(path + "/**/*.py")
        for file_path in files:
            if not os.path.isfile(file_path):
                continue
            try:
                m = load_module(file_path)
                self.register_models_in_module(m)
            except Exception as inst:
                traceback.print_exc()
                logging.error("Unable to load {0} error: {1}".format(file_path, inst))
                raise

    def register_endpoint(self, cls):
        methods = inspect.getmembers(cls, predicate=inspect.isfunction)

        path = cls.endpoint_path
        if path in self.endpoints:
            return
        self.endpoints[path] = {}

        logging.info("registering endpoint {0}".format(cls.__name__))

        api_class = APIClass(self, cls)
        self.endpoint_classes.append(api_class)

        for m in methods:
            met = m[1]
            if hasattr(met, "is_endpoint"):
                name = met.__name__
                self.endpoints[path][name] = APIMethod(api_class, met)

    def register_model(self, cls):
        if cls.__name__ not in self.models:
            logging.info("registering model {0}".format(cls.__name__))
            self.models[cls.__name__] = APIModel(self, cls)

    def find_model_by_name(self, name) -> APIModel:
        return self.models[name]

    def create_endpoint_instance(self, path, conn: 'APIConnection'):
        cmd = "/" + path.lstrip("/")
        epi = cmd.rindex("/") + 1
        eppath = cmd[:epi]
        ep = cmd[epi:]

        if eppath in self.endpoints:
            if ep in self.endpoints[eppath]:
                return self.endpoints[eppath][ep]

    def process_input_params(self, api_method: APIMethod, data):
        params_fields = api_method.params
        if params_fields is None:
            return {"data": data}
        if not isinstance(params_fields, list):
            params_fields = [params_fields]

        ctx = APISerializationContext()
        inctx = APIInternalSerializationContext(ctx)

        args = {}
        self.serializer.convert_obj_fields_from_user(inctx, params_fields, data, args)
        return args

    def process_output_value(self, ctx: BaseAPIRequestContext, api_method: APIMethod, data):
        serialization_ctx = APISerializationContext()
        if isinstance(data, APISerializationContext):
            serialization_ctx = data
            data = data._data

        inctx = APIInternalSerializationContext(serialization_ctx)
        inctx.request_ctx = ctx

        response_fields = api_method.response
        if response_fields is None:
            return data
        if not isinstance(response_fields, list):
            response_fields = [response_fields]

        if len(response_fields) == 1 and response_fields[0].name is None:
            field = response_fields[0]
            if isinstance(field, APIListField):
                return [self.serializer.transform_api_field_to_user(inctx, field.item_type, x)
                        for x in data]

            elif isinstance(field, APIField):
                return self.serializer.transform_api_field_to_user(inctx, field, data)

        args = {}
        self.serializer.convert_obj_fields_to_user(inctx, response_fields, data, args)
        return args

    def serialize_model(self, obj):
        t = type(obj)
        ctx = APISerializationContext()
        inctx = APIInternalSerializationContext(ctx)
        model = APIModel(self, t)
        return model.convert_obj(inctx, obj)

    def deserialize_model(self, t: Type[T], data) -> T:
        ctx = APISerializationContext()
        inctx = APIInternalSerializationContext(ctx)
        model = APIModel(self, t)
        return model.convert_obj_from_user(inctx, data)

    # noinspection SpellCheckingInspection
    def get_hashids(self, ns=None):
        if ns is None:
            return self.hashids

        if ns in self.hashids_ns:
            return self.hashids_ns[ns]
        else:
            h = Hashids(salt=self.secret_key + "-" + ns, min_length=16)
            self.hashids_ns[ns] = h
            return h

    def security_make_hash(self, x: int, namespace=None) -> str:
        return self.get_hashids(namespace).encode(x)

    def security_from_hash(self, x: str, namespace=None) -> int:
        from .api_connection import APINoPermissionsException
        d = self.get_hashids(namespace).decode(x)
        if len(d) == 0:
            raise APINoPermissionsException
        return d[0]

    async def handler(self, request: Request):
        from aiohttp import web
        from .api_connection import APIConnection

        ws = web.WebSocketResponse()
        try:
            await ws.prepare(request)
        except ConnectionResetError:
            return

        conn = APIConnection(self, request, ws)
        self.connections.append(conn)
        try:
            await conn.run()
        except (CancelledError, ConnectionResetError, APIInvalidLockException):
            pass
        finally:
            self.connections.remove(conn)
            self.lock_manager.release_conn(conn)
            self.changes_manager.remove_connection(conn)
        return ws

    @staticmethod
    def create_instance(secret_key: str) -> 'APIManager':
        APIManager.instance = APIManager(secret_key)
        return APIManager.instance

    @staticmethod
    def get_instance() -> 'APIManager':
        return APIManager.instance

    def initialize(self):
        for model in self.models.values():
            model.initialize()

        for a in self.endpoints.values():
            for api_method in a.values():
                api_method.initialize(self)

    def broadcast_message(self, data: Any):
        for conn in self.get_connection():
            conn.send_notification("broadcast", data)
