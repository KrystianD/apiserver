import re
from aiohttp.web_request import Request
from typing import Match


def camelcase2underscore(x):
    def fn(m: Match):
        txt = m.group(0).lower()
        if m.end(0) == len(m.string):  # split group of uppercase letters
            return "_" + txt
        else:
            return "_" + (txt[:-1] + "_" + txt[-1]).strip("_")

    return re.sub("[A-Z]+", fn, x).lstrip("_")


def path_to_camelcase(x):
    x = re.sub("[\/_].", lambda x: x.group(0)[1].upper(), x)
    x = x[0].lower() + x[1:]
    return x


def get_client_ip(request: Request, trusted_proxies=('127.0.0.1')):
    def get_peername_host():
        peername = request.transport.get_extra_info('peername')

        if peername is not None:
            host, port = peername
            return host
        else:
            return None

    client_ip = get_peername_host()

    if trusted_proxies:
        if client_ip not in trusted_proxies:
            raise Exception("not trusted proxy: {}".format(client_ip))

        if "X-Forwarded-For" in request.headers:
            return request.headers["X-Forwarded-For"]

    return client_ip
