import datetime
import decimal
from typing import List, Union, Type, Any, TYPE_CHECKING

import types
import base64
import enum
from bson import ObjectId

from .api_serialization_context import APIInternalSerializationContext
from .types import APIFieldFlags
from .api_file import APIFile
from .types import APIModel, APIFieldType, APIField, APIListField, APIMapField, \
    APIModelFlags

if TYPE_CHECKING:
    from .api_manager import APIManager


class APITypeConversionException(Exception):
    def __init__(self, message):
        super().__init__(message)


def check_type(field, value, expected_types: Union[Type, List[Type]]):
    if not isinstance(expected_types, list):
        expected_types: List[Type] = [expected_types]
    # noinspection PyTypeChecker
    if not any(isinstance(value, x) for x in expected_types):
        msg = "Invalid type for {0}, got {1}, expected: {2}, value: {3}".format(
                field,
                type(value),
                expected_types,
                value)
        raise APITypeConversionException(msg)


class Serializer:
    def __init__(self, mgr: 'APIManager'):
        self.mgr = mgr

    def should_convert_to_user(self, ctx: APIInternalSerializationContext, field: APIField):
        # noinspection PyProtectedMember
        if field.groups & ctx.user_ctx._excluded_groups:
            return False

        return self.should_convert(ctx, field)

    def should_convert_from_user(self, ctx: APIInternalSerializationContext, field: APIField):
        return self.should_convert(ctx, field)

    # noinspection PyMethodMayBeStatic
    def should_convert(self, ctx: APIInternalSerializationContext, field: APIField):
        if ctx.request_ctx is not None and field.permissions is not None:
            if not ctx.request_ctx.has_permission(field.permissions):
                return False

        if field.type == APIFieldType.Key:
            return True

        elif field.type == APIFieldType.Integer:
            return True

        elif field.type == APIFieldType.Float:
            return True

        elif field.type == APIFieldType.String:
            return True

        elif field.type == APIFieldType.Boolean:
            return True

        elif field.type == APIFieldType.Binary:
            return True

        elif field.type == APIFieldType.Decimal:
            return True

        elif field.type == APIFieldType.DateTime:
            return True

        elif field.type == APIFieldType.Date:
            return True

        elif field.type == APIFieldType.Enum:
            return True

        elif field.type == APIFieldType.JSON:
            return True

        elif field.type == APIFieldType.File:
            return True

        elif isinstance(field, APIListField):
            if field.item_type.type == APIFieldType.Custom:
                item_model = field.item_type.custom_type
                assert isinstance(item_model, APIModel)
                if item_model.name in ctx.converted_types and APIModelFlags.AllowRecursion not in item_model.flags:
                    return False

            return True

        elif isinstance(field, APIMapField):
            if field.value_type.type == APIFieldType.Custom:  # only check value type
                item_model = field.value_type.custom_type
                assert isinstance(item_model, APIModel)
                if item_model.name in ctx.converted_types and APIModelFlags.AllowRecursion not in item_model.flags:
                    return False

            return True

        elif field.type == APIFieldType.Custom:
            obj_model = field.custom_type
            assert isinstance(obj_model, APIModel)

            if obj_model.name in ctx.converted_types and APIModelFlags.AllowRecursion not in obj_model.flags:
                return False

            return True

        else:
            raise ValueError("no should_convert for {0}".format(field.type.name))

    def transform_api_field_from_user(self, ctx: APIInternalSerializationContext, field: APIField, value):
        if value is None:
            return None

        if field.type == APIFieldType.Key:
            return self.mgr.security_from_hash(value)

        elif field.type == APIFieldType.Integer:
            check_type(field, value, int)
            return value

        elif field.type == APIFieldType.Float:
            check_type(field, value, [float, int])
            return float(value)

        elif field.type == APIFieldType.String:
            check_type(field, value, str)
            return value

        elif field.type == APIFieldType.Boolean:
            check_type(field, value, bool)
            return value

        elif field.type == APIFieldType.Binary:
            check_type(field, value, str)
            return base64.b64decode(value, validate=True)

        elif field.type == APIFieldType.Decimal:
            check_type(field, value, str)
            return decimal.Decimal(value)

        elif field.type == APIFieldType.DateTime:
            check_type(field, value, str)
            return datetime.datetime.strptime(value.rstrip("Z"), "%Y-%m-%dT%H:%M:%S")

        elif field.type == APIFieldType.Date:
            check_type(field, value, str)
            return datetime.datetime.strptime(value.rstrip("Z"), "%Y-%m-%d").date()

        elif field.type == APIFieldType.Enum:
            check_type(field, value, str)
            enum_cls = field.custom_type
            return enum_cls[value]

        elif field.type == APIFieldType.JSON:
            return value

        elif field.type == APIFieldType.File:
            check_type(field, value, dict)
            return APIFile.from_dict(value)

        elif isinstance(field, APIListField):
            check_type(field, value, list)

            out = []

            for item in value:
                out.append(self.transform_api_field_from_user(ctx, field.item_type, item))

            return out

        elif isinstance(field, APIMapField):
            check_type(field, value, list)

            out = {}

            for key, value in value:
                pkey = self.transform_api_field_from_user(ctx, field.key_type, value)
                pvalue = self.transform_api_field_from_user(ctx, field.value_type, value)
                out[pkey] = pvalue

            return out

        elif field.type == APIFieldType.Custom:
            check_type(field, value, dict)
            obj_model = field.custom_type
            assert isinstance(obj_model, APIModel)

            ctx.converted_types.append(obj_model.name)
            ret = obj_model.convert_obj_from_user(ctx, value)
            ctx.converted_types.pop()
            return ret

        else:
            raise ValueError("no from_user converter {0}".format(field.type.name))

    def transform_api_field_to_user(self, ctx: APIInternalSerializationContext, field: APIField, value):
        if value is None:
            return None

        if field.type == APIFieldType.Key:
            return self.mgr.security_make_hash(value)

        elif field.type == APIFieldType.Integer:
            check_type(field, value, int)
            return value

        elif field.type == APIFieldType.Float:
            check_type(field, value, [int, float])
            return float(value)

        elif field.type == APIFieldType.String:
            check_type(field, value, [str, ObjectId])
            return str(value)

        elif field.type == APIFieldType.Boolean:
            check_type(field, value, bool)
            return value

        elif field.type == APIFieldType.Binary:
            check_type(field, value, bytes)
            return base64.b64encode(value).decode("ascii")

        elif field.type == APIFieldType.Decimal:
            check_type(field, value, [int, decimal.Decimal])
            return str(value)

        elif field.type == APIFieldType.DateTime:
            check_type(field, value, datetime.datetime)
            return value.isoformat()

        elif field.type == APIFieldType.Date:
            check_type(field, value, datetime.date)
            return value.isoformat()

        elif field.type == APIFieldType.Enum:
            check_type(field, value, enum.Enum)
            return value.name

        elif field.type == APIFieldType.JSON:
            return value

        elif isinstance(field, APIListField):
            check_type(field, value, [list, types.GeneratorType])

            out = []
            for item in value:
                out.append(self.transform_api_field_to_user(ctx, field.item_type, item))

            return out

        elif isinstance(field, APIMapField):
            check_type(field, value, dict)

            out = []
            for key, value in value.items():
                out.append((self.transform_api_field_to_user(ctx, field.key_type, key),
                            self.transform_api_field_to_user(ctx, field.value_type, value)))

            return out

        elif field.type == APIFieldType.Custom:
            obj_model = field.custom_type
            assert isinstance(obj_model, APIModel)

            check_type(field, value, obj_model.cls)

            ctx.converted_types.append(obj_model.name)
            ret = obj_model.convert_obj(ctx, value)
            ctx.converted_types.pop()
            return ret

        else:
            raise ValueError("no to_user converter {0}".format(field.type.name))

    def convert_obj_fields_to_user(self, ctx: APIInternalSerializationContext, fields: List[APIField], data: Any,
                                   out_data: Any) -> None:
        if isinstance(data, dict):
            input_get = lambda x: data.get(x)
        else:
            input_get = lambda x: getattr(data, x)

        if isinstance(out_data, dict):
            output_set = lambda x, y: out_data.__setitem__(x, y)
        else:
            output_set = lambda x, y: setattr(out_data, x, y)

        for field in fields:
            if self.should_convert_to_user(ctx, field):
                if field.getter is not None:
                    model_value = field.getter(data)
                else:
                    model_value = input_get(field.name)
                out_value = self.transform_api_field_to_user(ctx, field, model_value)
                output_set(field.name, out_value)

    def convert_obj_fields_from_user(self, ctx: APIInternalSerializationContext, fields: List[APIField], data: Any,
                                     out_data: Any) -> None:
        if isinstance(data, dict):
            input_get = lambda x: data.get(x)
        else:
            input_get = lambda x: getattr(data, x)

        if isinstance(out_data, dict):
            output_set = lambda x, y: out_data.__setitem__(x, y)
        else:
            output_set = lambda x, y: setattr(out_data, x, y)

        for field in fields:
            name = field.name

            if APIFieldFlags.ToClientOnly in field.flags:
                continue

            if self.should_convert_from_user(ctx, field):
                user_value = input_get(name)
                conv_value = self.transform_api_field_from_user(ctx, field, user_value)
                if field.setter is not None:
                    field.setter(out_data, conv_value)
                else:
                    output_set(name, conv_value)
