from typing import Any, Set

from .api_request_context import BaseAPIRequestContext


class APISerializationContext:
    def __init__(self, data: Any = None) -> None:
        self._data = data
        self._excluded_groups: Set[Any] = set()

    def exclude_group(self, group: Any):
        self._excluded_groups.add(group)
        return self

    def exclude_groups(self, *args):
        for x in args:
            self.exclude_group(x)
        return self


class APIInternalSerializationContext:
    def __init__(self, user_ctx: APISerializationContext) -> None:
        self.user_ctx = user_ctx
        self.converted_types = []
        self.request_ctx: BaseAPIRequestContext = None
