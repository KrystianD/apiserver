from typing import TYPE_CHECKING, Union, Dict, Set

from enum import Enum

if TYPE_CHECKING:
    from .api_manager import APIManager
    from .api_connection import APIConnection


class APIChangeKey:
    def __init__(self, object_type: Union[str, Enum], object_id: Union[str, int]):
        if isinstance(object_type, Enum):
            object_type = object_type.name

        if isinstance(object_id, int):
            object_id = str(object_id)

        self.object_type: str = object_type
        self.object_id: str = object_id

    def __hash__(self):
        return hash((self.object_type, self.object_id))

    def __eq__(self, other):
        return (self.object_type, self.object_id) == (other.object_type, other.object_id)

    def __ne__(self, other):
        return not (self == other)


class APIChangesManager:
    def __init__(self, manager: 'APIManager') -> None:
        self.manager = manager

        self.subscriptions: Dict['APIConnection', Set[APIChangeKey]] = {}

    def notify_change(self, key: APIChangeKey):
        pass

    def subscribe(self, conn: 'APIConnection', key: APIChangeKey):
        if conn not in self.subscriptions:
            self.subscriptions[conn] = set()

        self.subscriptions[conn].add(key)

    def remove_connection(self, conn: 'APIConnection'):
        if conn in self.subscriptions:
            del self.subscriptions[conn]
