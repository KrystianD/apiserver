from .api_endpoint import APIEndpoint
from .api_lock_manager import APILock
from .api_request_context import BaseAPIRequestContext
from .decorators import endpoint, method, params, response
from .types.api_field import APIFieldType, APIField, APIListField


@endpoint("/mgmt")
class MgmtEndpoint(APIEndpoint):
    @method
    @params(
        APIField("object_type", APIFieldType.String)
    )
    @response(APIListField("locks", APIFieldType.Custom, APILock))
    async def watch_locks(self, ctx: BaseAPIRequestContext, object_type: str):
        if not ctx.has_permission_to_lock(object_type):
            raise PermissionError("no perm")

        ctx.conn.add_locks_watcher(object_type)

        return {
            "locks": ctx.conn.api_manager.lock_manager.get_locks_by_object_type(object_type)
        }

    @method
    @params(APIField("lock_id", APIFieldType.String))
    async def release_lock(self, ctx: BaseAPIRequestContext, lock_id: str):
        ctx.conn.api_manager.lock_manager.release_lock(lock_id, ctx.conn)
