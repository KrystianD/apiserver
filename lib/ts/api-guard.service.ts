import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanActivate } from '@angular/router';

import { APIService, APIConnectionState } from './api.service';

@Injectable()
export class APIGuard implements CanActivate, CanActivateChild {
  constructor(private apiService: APIService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let hasLoginTrue = false;
    let hasLoginFalse = false;

    for(let part of route.pathFromRoot.reverse())
    {
      if (part.data["loginRequired"] === true)
        hasLoginTrue = true;
      if (part.data["loginRequired"] === false)
        hasLoginFalse = true;
    }

    let loginRequired = hasLoginTrue || (!hasLoginTrue && !hasLoginFalse);

    if (loginRequired) {
      if (this.apiService.isConnected())
        return this.apiService.isAuthenticated();
      else
        return this.apiService.getStateChangeEvent()
          .map(x => x === APIConnectionState.Connected);
    } else {
      if (this.apiService.isConnected())
        return true;
      else
        return this.apiService.getStateChangeEvent()
          .map(x => x === APIConnectionState.Connected ||
            x === APIConnectionState.ConnectedNotAuthorized);
    }
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
