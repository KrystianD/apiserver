// AUTOGENERATED, DO NOT TOUCH
import * as Rx from 'rxjs';
import Decimal from 'decimal.js';
import * as moment from 'moment';
import { Moment } from 'moment';

import { List } from 'kdlib';

import * as BaseModels from './api.models.gen';
import { APIFile } from './api.models.gen';

function string_fromServer(x) {
  if (x === null) return null;
  return x;
}
function string_toServer(x) {
  if (x === null) return null;
  return x;
}

function int_fromServer(x) {
  if (x === null) return null;
  return x;
}
function int_toServer(x) {
  if (x === null) return null;
  return Math.floor(x);
}

function float_fromServer(x) {
  if (x === null) return null;
  return x;
}
function float_toServer(x) {
  if (x === null) return null;
  return x;
}

function any_fromServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  return x;
}
function any_toServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  return x;
}

function boolean_fromServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  return x;
}
function boolean_toServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  return x;
}

function DateTime_fromServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  x = moment.utc(x);
  return x;
}
function DateTime_toServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  x = x.utc().format();
  return x;
}

function Date_fromServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  x = moment.utc(x);
  return x;
}
function Date_toServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  x = x.utc().format("YYYY-MM-DD");
  return x;
}

function Decimal_fromServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  x = new Decimal(x);
  return x;
}
function Decimal_toServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  x = x.toString();
  return x;
}

function Uint8Array_fromServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  let binary = atob(x);
  x = new Uint8Array(binary.length);
  for (let i = 0; i < binary.length; i++)
    x[i] = binary.charCodeAt(i);
  return x;
}
function Uint8Array_toServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  let b = "";
  const len = x.byteLength;
  for (let i = 0; i < len; i++)
    b += String.fromCharCode(x[i]);
  x = btoa(b);
  return x;
}

function List_fromServer<T>(x, itemConverter) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  let obj = new List<T>();
  for (let item of x)
      obj.push(itemConverter(item));
  return obj;
}
async function List_toServer(x, itemConverter) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  let obj = [];
  for (let item of x)
      obj.push(await itemConverter(item));
  return obj;
}

function Map_fromServer<K,V>(x, keyConverter, itemConverter) {
  if (x === null) return null;
  let obj = new Map<K,V>();
  for (let item of x)
      obj.set(keyConverter(item[0]), itemConverter(item[1]));
  return obj;
}
async function Map_toServer(x, keyConverter, itemConverter) {
  if (x === null) return null;
  let obj = [];
  for (let [key, value] of x)
      obj.push(await keyConverter(key), await itemConverter(value));
  return obj;
}

function Enum_fromServer(T, x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  let obj = T.fromString(x);
  return obj;
}
async function Enum_toServer(T, x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  let obj = x.getName();
  return obj;
}

function APIFile_fromServer(x) {
  if (x === null) return null;
  if (x === undefined) return undefined;
  return x;
}

async function APIFile_toServer(x: APIFile) {
  if (x === null) return null;
  if (x === undefined) return undefined;

  let pr = new Promise((resolve) => {
    let fileReader = new FileReader();
    fileReader.onload = function (event: any) {
      resolve(Uint8Array_toServer(new Uint8Array(event.target.result)));
    };
    fileReader.readAsArrayBuffer(x as any);
  });

  return {
    name: x.name,
    type: (x as any).type,
    moddate: (x as any).lastModifiedDate,
    data: await pr,
  };
}

function any_clone<T extends BaseModels.ICloneable>(val: T, creator)
{
  if (val === null) return null;
  if (val === undefined) return undefined;
  let obj = creator();
  val.cloneTo(obj);
  return obj;
}

function any_deepClone<T extends BaseModels.ICloneable>(val: T, creator)
{
  if (val === null) return null;
  if (val === undefined) return undefined;
  let obj = creator();
  val.deepCloneTo(obj);
  return obj;
}

function Moment_clone(val)
{
  if (val === null) return null;
  if (val === undefined) return undefined;
  return val.clone();
}

function Uint8Array_clone(val)
{
  return new Uint8Array(val);
}

function List_clone<T>(list: List<T>, itemCloner)
{
  if (list === null) return null;
  if (list === undefined) return undefined;
  let newList = new List<T>();
  for (let item of list)
    newList.push(itemCloner(item));
  return newList;
}

function Map_clone<K,V>(map: Map<K,V>, keyCloner, valueCloner)
{
  if (map === null) return null;
  if (map === undefined) return undefined;
  let newMap = new Map<K,V>();
  for (let [key, value] of map)
    newMap.set(keyCloner(key), valueCloner(value));
  return newMap;
}

%TYPES_CONVERTERS%

%CLASS_CONVERTERS%

%CLONERS%

export class APIData {
%DATA_FIELDS%
}

export enum APIConnectionState {
  Idle,
  Connecting,
  Connected,
  ConnectedNotAuthorized,
  VersionMismatch,
}

%ENDPOINTS%

export class APIServerUserErrorException extends Error {
  constructor(m: string) {
    super(m);
    Object.setPrototypeOf(this, APIServerUserErrorException.prototype);
  }
}

export abstract class BaseAPIService {
  private token: string;

  private ws: WebSocket;
  private packetNum = 1;

  private reconnectAttempt = 0;
  private state: APIConnectionState;
  public currentUser: User = null;
  public stateChangeEvent: Rx.Subject<APIConnectionState> = new Rx.Subject<APIConnectionState>();

  private responseListeners = new Map<number, Rx.Subject<any>>();

  private pingSub: Rx.Subscription = null;

  constructor() {
    this.token = localStorage.getItem("api_token");
    this.state = APIConnectionState.Idle;
  }

  protected abstract getWebsocketUrl(): string;
  protected abstract isDebugMode(): boolean;
  protected abstract getBuildRevision(): string;
  protected abstract getAuth(): any;

  public connect() {
    this.ws = new WebSocket(this.getWebsocketUrl());
    this.ws.onopen = (x) => this.onOpen(x);
    this.ws.onmessage = (x) => this.onMessage(x);
    this.ws.onclose = (x) => this.onClose(x);
  }

  public isConnected() {
    return this.state === APIConnectionState.Connected || this.state === APIConnectionState.ConnectedNotAuthorized;
  }

  private getAuthInner() {
    let token = localStorage.getItem("api_token");
    if (token !== null && token !== undefined)
      return { "token": token };
    else
      return this.getAuth();
  }

  public cmd(cmd: string, params: any): Rx.Observable<any> {
    if (cmd !== "auth" && !this.isConnected()) {
      return Rx.Observable.throw("not-connected");
    }
    let packet = {
      num: this.packetNum,
      cmd,
      params,
    };
    this.log("cmd", packet);
    let jsonData = JSON.stringify(packet);
    this.ws.send(jsonData);
    let subject = new Rx.Subject<any>();
    this.responseListeners.set(this.packetNum, subject);
    this.packetNum++;
    return subject.asObservable().timeout(60000);
  }

  public hasAuth() {
    return this.getAuthInner() !== null;
  }

  private onOpen(event: Event) {
    this.log("connection established");
  }

  public watchedLocks = new Map<string, Rx.Subject<List<APILock>>>();
  public broadcastListeners = new Rx.Subject<any>();
  public locks = new Map<string, List<APILock>>();
  public idToLock = new Map<string, APILock>();

  public listenToBroadcast(): Rx.Observable<any> {
    return this.broadcastListeners.asObservable();
  }

  public watchLocks(object_type: string): Rx.Observable<List<APILock>> {
    if (this.watchedLocks.has(object_type)) {
      return Rx.Observable.of(new List<APILock>());
    } else {
      let subj = new Rx.Subject<List<APILock>>();
      this.watchedLocks.set(object_type, subj);

      this.setupLocksWatcher(object_type).subscribe(locks => {
        subj.next(locks);
      });
      return subj.asObservable();
    }
  }

  public releaseLock(lock: APILock | string) {
    if (lock instanceof APILock)
      lock = lock.id;

    this.MgmtEndpoint.releaseLock(lock).subscribe(x => {
    });
  }

  private setupLocksWatcher(object_type: string) {
    this.locks.set(object_type, new List<APILock>());
    return this.MgmtEndpoint.watchLocks(object_type).map(x => {
      let locks = x.locks;
      this.locks.set(object_type, locks);
      for (let lock of locks)
        this.idToLock.set(lock.id, lock);
      return locks;
    });
  }

  private doAuth() {
    let auth = this.getAuthInner();

    this.log("performing authentication", auth);

    this.currentUser = null;
    if (auth !== null) {
      this.cmd("auth", auth).subscribe(
        user => {
          this.currentUser = User_fromServer(user);
          this.log("connected with user", user);
          this.reconnectAttempt = 0;
          this.state = APIConnectionState.Connected;
          this.stateChangeEvent.next(this.state);
        },
        error => {
          this.log("authentication error", error);
          this.reconnectAttempt = 0;
          this.state = APIConnectionState.ConnectedNotAuthorized;
          this.stateChangeEvent.next(this.state);
        });
    } else {
      this.log("connected without auth");
      this.reconnectAttempt = 0;
      this.state = APIConnectionState.ConnectedNotAuthorized;
      this.stateChangeEvent.next(this.state);
    }

    this.pingSub = Rx.Observable.timer(5000, 5000).subscribe(x => {
      let packet = {
        cmd: "ping",
      };
      this.ws.send(JSON.stringify(packet));
    });
  }

  private onMessage(event: MessageEvent) {
    let packet = JSON.parse(event.data);
    this.log("onMessage", packet);
    switch (packet["type"]) {
      case "info":
        let serverRev = packet["server_revision"];
        let clientRev = this.getBuildRevision();
        if (!this.isDebugMode() && clientRev !== serverRev) {
          this.log(`Revision mismatch, server: ${serverRev}, client: ${clientRev}`);
          this.state = APIConnectionState.VersionMismatch;
          this.stateChangeEvent.next(this.state);
          this.ws.close(1000, "version-mismatch");
        } else {
          this.doAuth();
        }
        break;
      case "response":
        let num = packet["num"];
        let data = packet["response"];
        let result = packet["result"];
        if (this.responseListeners.has(num)) {
          let subject = this.responseListeners.get(num);
          if (result === "ok") {
            subject.next(data);
            subject.complete();
          } else if (result === "error") {
            subject.error(new Error(result));
            subject.complete();
          } else if (result === "user-error") {
            subject.error(new APIServerUserErrorException(packet["message"]));
            subject.complete();
          }
          this.responseListeners.delete(num);
        }
        break;
      case "notification":
        let payload = packet["payload"];
        switch (packet["ntype"]) {
          case "lock":
            let type = payload["type"];
            switch (type) {
              case "acquired": {
                let lock = APILock_fromServer(payload["lock"]);
                this.locks.get(lock.object_type).push(lock);
                this.idToLock.set(lock.id, lock);
                this.log("lock acquired", lock);
                this.watchedLocks.get(lock.object_type).next(this.locks.get(lock.object_type));
                break;
              }
              case "released": {
                let lock_id = payload["lock_id"];
                let lock = this.idToLock.get(lock_id);
                this.idToLock.delete(lock.id);
                this.locks.get(lock.object_type).remove(lock);
                this.log("lock released", lock_id);
                this.watchedLocks.get(lock.object_type).next(this.locks.get(lock.object_type));
                break;
              }
            }
            break;
          case "broadcast":
            this.broadcastListeners.next(payload);
            break;
        }
        break;
    }
  }

  private onClose(event: CloseEvent) {
    this.log("onClose", event);

    if (event.reason === "version-mismatch" || this.state === APIConnectionState.VersionMismatch)
      return;

    for (let subject of Array.from(this.responseListeners.values())) {
      subject.error(new Error());
      subject.complete();
    }
    this.responseListeners.clear();

    this.locks.clear();
    this.idToLock.clear();

    if (this.pingSub !== null) {
      this.pingSub.unsubscribe();
      this.pingSub = null;
    }

    this.state = this.state = APIConnectionState.Connecting;
    this.stateChangeEvent.next(this.state);
    this.log(`state: Connecting (attempts: ${this.reconnectAttempt})`);

    Rx.Observable.timer(2000).subscribe(x => {
      this.connect();
    });

    this.reconnectAttempt++;
  }

  private log(txt, ...optionalParams: any[]) {
    if (this.isDebugMode())
      console.log.apply(null, [`[WS] ${txt}`].concat(optionalParams));
  }

  public getStateChangeEvent() {
    return this.stateChangeEvent;
  }

  public isAuthenticated() {
    return this.currentUser !== null;
  }

  public init() {
    this.connect();
  }

  doLogin(username: string, password: string, app: any): Rx.Observable<string> {
    let p = new LoginParams();
    p.username = username;
    p.password = password;
    p.app = app;
    return this.AuthEndpoint.login(p)
      .map(x => {
        switch (x.result) {
          case "ok":
            this.token = x.token;
            this.currentUser = x.user;
            localStorage.setItem("api_token", this.token);
            this.state = APIConnectionState.Connected;
            this.stateChangeEvent.next(this.state);
            return null;
          default:
            return x.error_message;
        }
      });
  }

  public logout() {
    this.token = null;
    this.currentUser = null;
    localStorage.removeItem("api_token");
    return this.AuthEndpoint.deauth();
  }

%CMDS%
}
