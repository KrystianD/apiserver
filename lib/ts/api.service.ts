export * from './api.service.gen';
import { BaseAPIService } from './api.service.gen';

import { Injectable } from '@angular/core';

@Injectable()
export class APIService extends BaseAPIService {
  constructor() {
    super();
  }

  protected getWebsocketUrl(): string {
  }
  protected isDebugMode(): boolean {
  }
  protected getBuildRevision(): string {
  }
  protected getAuth(): any {
  }
}
